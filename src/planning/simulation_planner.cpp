#include "planning/simulation_planner.h"

// When you emit a signal from thread A, the slot it is connected to runs in thread B
CannulaSimulation::Planner::Planner()
    : set_u_z_(false) {
  previously_planned_goal_point_.resize(0);
  moveToThread(this);
}

CannulaSimulation::Planner::~Planner() {
  quit();
  wait();
}

void CannulaSimulation::Planner::run() {
  exec();
}

// returns whether we have a new goal point with
// different x, y, and z coordinates than those of
// the previously planned point
bool CannulaSimulation::Planner::is_point_a_new_goal_point(
                                     const Eigen::Vector3d* new_goal_point) const {
  return !(are_vectors_equal(previously_planned_goal_point_, *new_goal_point));
}

bool CannulaSimulation::Planner::are_vectors_equal(
                                     const Eigen::Vector3d& vector_one,
                                     const Eigen::Vector3d& vector_two) const {
  if (vector_one.size() != vector_two.size()) { return false;} 

  for (size_t i = 0; i < vector_one.size(); ++i) {
    if (vector_one(i) != vector_two(i)) {
      return false;
    }
  }
  return true;
}

// this slot is called whenever the visualization
// thread indicates that we should be planning for
// a new point given by the transformed coordinates 
// of the phantom
void CannulaSimulation::Planner::update_motion_plan_for_new_goal(Eigen::Vector3d *goal_point) {
  if (is_point_a_new_goal_point(goal_point)) {
    if (current_motion_plan_.path_) {
      current_motion_plan_.clearPlan();
    }
    // Get the collision-free path and populate the queue.
    cannula_planning_->getMotionPlanToPoint((*goal_point)[0],
                                            (*goal_point)[1],
                                            (*goal_point)[2],
                                            &current_motion_plan_);
    // if the initial u_z has not been set in
    // the simulated cannula, intialize it
    if (!set_u_z_) {
      simulated_cannula_->set_current_u_z(current_motion_plan_.getShape(0)->getBVPSoln());
      set_u_z_ = true;
      simulated_cannula_->turn_on_robot();
    }

    send_new_motion_plan();
    // update the previousl planned goal_point
    previously_planned_goal_point_ = *goal_point;
    delete goal_point;
  }
}

void CannulaSimulation::Planner::send_new_motion_plan() {
  // if we have a new plan ready, emit
  // a signal with the new plan as the
  // argument
  std::deque<StateInformation*> *state_deque = nullptr;

  if (current_motion_plan_.path_) {
    state_deque = new std::deque<StateInformation *>;
    for (size_t state_index = 0; state_index < current_motion_plan_.getNumStates(); ++state_index) {
      // new structinformation object to hold alpha and beta at state
      // needs to store alpha/beta for each tube at this state
      StateInformation *new_state = new StateInformation(num_tubes_);

      for (size_t tube_index = 0; tube_index < num_tubes_; ++tube_index) {
        new_state->add_alpha_to_state(tube_index, current_motion_plan_.getAlpha(state_index, tube_index));
        new_state->add_beta_to_state(tube_index, current_motion_plan_.getBeta(state_index, tube_index));
      }

      state_deque->push_back(new_state);
    } // end iteration through states
  }

  emit notify_configurations_queue_manager(state_deque);
}