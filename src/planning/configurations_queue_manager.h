#ifndef CANNULA_VISUALIZATION_SIMULATION_CONFIGURATIONS_QUEUE_MANAGER_H_
#define CANNULA_VISUALIZATION_SIMULATION_CONFIGURATIONS_QUEUE_MANAGER_H_
#include "simulation/simulation.h"

namespace CannulaSimulation {
class ConfigurationsQueueManager : public QThread {
 Q_OBJECT
 public:
  ConfigurationsQueueManager();
  ~ConfigurationsQueueManager();

 protected:
  void run();

 private:
  const size_t kUpdatesPerSecond;
  static const size_t kMillisecondsPerSecond = 1e3;
  static const size_t kStopPlanningQueueSizeTolerance = 0;
  // Double-ended queue for its dynamic size and clear() function
  // still guarantees O(1) amortized time for insertion, and deletion.
  std::deque<StateInformation *> configurations_queue_;
  QTimer timer_;
  bool need_to_send_next_configuration_;

 private slots:
  void update_queue_with_plan(std::deque<StateInformation *> *new_configurations_queue);
  void get_next_configuration();
  void update();

 signals:
  void send_next_configuration(StateInformation state_information);
  void set_planning(bool planning);
};

} // namespace CannulaSimulation

#endif // CANNULA_VISUALIZATION_SIMULATION_CONFIGURATIONS_QUEUE_MANAGER_H_