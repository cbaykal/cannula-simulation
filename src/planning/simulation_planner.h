#ifndef CANNULA_VISUALIZATION_PLANNING_SIMULATION_PLANNER_H_
#define CANNULA_VISUALIZATION_PLANNING_SIMULATION_PLANNER_H_

#include <memory>
#include "simulation/simulation.h"

namespace CannulaSimulation {
class Planner: public QThread {
Q_OBJECT
 public:
  Planner();
  ~Planner();

  void set_cannula_planning(
      const std::shared_ptr<CannulaPlanning> cannula_planning) { 
    cannula_planning_ = cannula_planning;
  }

  void set_simulated_cannula(
      SimulatedCannula *simulated_cannula) {
    simulated_cannula_ = simulated_cannula;
  }
  void set_num_tubes(size_t num_tubes) { num_tubes_ = num_tubes; }

 protected:
  void run();

 private:
  bool is_point_a_new_goal_point(const Eigen::Vector3d* new_goal_point) const;
  // TODO: Doesn't Eigen have a way to compare vectors?
  bool are_vectors_equal(const Eigen::Vector3d& vector_one,
                         const Eigen::Vector3d& vector_two) const;
  void send_new_motion_plan();
  CannulaPlanning::CannulaMotionPlan current_motion_plan_; 
  std::shared_ptr<CannulaPlanning> cannula_planning_;
  // Pointer to SimulatedCannula that does NOT need to be deallocated in the
  // destructor. The SimulatedCannula instance is a member of the GLWidget
  // object and will be deallocated automatically when GLWidget is destroyed.
  SimulatedCannula *simulated_cannula_;

  volatile bool set_u_z_;
  size_t num_tubes_;
  Eigen::Vector3d previously_planned_goal_point_;

 private slots:
  // called by GLWidget (visualization thread) whenever a
  // new goal is obtained
  void update_motion_plan_for_new_goal(Eigen::Vector3d *goal_point);

 signals:
  void notify_configurations_queue_manager(
           std::deque<StateInformation*>* new_configurations_queue);

};
} // namespace CannulaSimulation

#endif // CANNULA_VISUALIZATION_PLANNING_SIMULATION_PLANNER_H_