#include "planning/configurations_queue_manager.h"

CannulaSimulation::ConfigurationsQueueManager::ConfigurationsQueueManager()
    : need_to_send_next_configuration_(false),
      kUpdatesPerSecond(200),
      timer_(this) {
  moveToThread(this);
}

CannulaSimulation::ConfigurationsQueueManager::~ConfigurationsQueueManager() {
  // Deallocate the pointers inside the vector to prevent memory leaks.
  auto begin = configurations_queue_.begin();
  auto end = configurations_queue_.end();
  for (auto it = begin; it!= end; ++it) {
    delete (*it);
  }

  quit();
  wait();
}

void CannulaSimulation::ConfigurationsQueueManager::run() {
  connect(&timer_, SIGNAL(timeout()), this, SLOT(update()));
  timer_.start(kMillisecondsPerSecond/kUpdatesPerSecond);
  exec();
}

void CannulaSimulation::ConfigurationsQueueManager::update_queue_with_plan(
                     std::deque<StateInformation*> *new_configurations_queue) {
  if (new_configurations_queue) {
    // Enqueue all of the new states retrieved from the new motion plan to the 
    // end of the queue.
    configurations_queue_.insert(configurations_queue_.end(),
                                 new_configurations_queue->begin(),
                                 new_configurations_queue->end());
                                        
    delete new_configurations_queue;
  }
}

void CannulaSimulation::ConfigurationsQueueManager::update() {
  if (need_to_send_next_configuration_) {
    get_next_configuration();
  }

  // The queue might be "too full" and keep increasing in 
  // size uncontrollably avoid planning  when we have plenty of 
  // configurations to simulate already
  if (configurations_queue_.size() > kStopPlanningQueueSizeTolerance){
    emit set_planning(false);
  } else {
    emit set_planning(true);
  }
}

void CannulaSimulation::ConfigurationsQueueManager::get_next_configuration() {
  if (!configurations_queue_.empty()) {
    // pass along the configuration at the front
    // of the queue to the simulation thread
    emit send_next_configuration(*(configurations_queue_.front()));
    configurations_queue_.pop_front();
    need_to_send_next_configuration_ = false;
  } else {
    // we can't send the next configuration right now
    // since we currently don't have a populated queue
    // but keep this request in mind
    need_to_send_next_configuration_ = true;
  }
}
