#ifndef CANNULA_VISUALIZATION_SIMULATION_REAL_CANNULA_H_
#define CANNULA_VISUALIZATION_SIMULATION_REAL_CANNULA_H_
#include "interfaces/abstract_cannula_interface.h"
#include "util/state_information.h"
#include "interfaces/abstract_motor_interface.h"
#include <Qt/qthread.h>
#include <Qt/qtimer.h>
#include <memory>

namespace RealCannula {
class Cannula: public AbstractCannulaInterface {
Q_OBJECT
 public:
  Cannula();
  ~Cannula();

  /*
  * Mutators
  */
  virtual void turn_on_robot() { 
    is_robot_running_ = true;
    start();
  }

  virtual void turn_off_robot() { is_robot_running_ = false; }

  /*
  * Accessors
  */
  virtual bool is_robot_running() const { return is_robot_running_; }

  virtual void add_motor(double max_motor_speed);
  virtual void print_current_state() const;

 protected:
  virtual void run();

 private: 
  static const size_t kMillisecondsPerSecond = 1e3;
  static const size_t kMotorUpdatesPerSecond = 200;
  void advance_motor_positions(double elapsed_time_milliseconds);
  bool are_all_position_goals_reached() const;
  void set_motor_goal_positions();

  bool is_robot_running_;
  bool have_set_motor_distance_goals_;
  bool asked_for_goal_state_;
  double motor_update_interval_milliseconds_;
  size_t motors_per_tube_;

  StateInformation goal_state_;
  std::vector<AbstractMotorInterface *> motors_vector_;
  std::shared_ptr<QTimer> motors_update_timer_;
 private slots:
   virtual void new_goal_configuration(StateInformation goal_state);
  // update RealMotor states here
  void update_motor_states();
  
 signals:
  void need_goal_configuration();
};
} // namespace GenuineCannula

#endif // CANNULA_VISUALIZATION_REAL_CANNULA_H_