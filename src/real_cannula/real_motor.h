#ifndef CANNULA_VISUALIZATION_REAL_CANNULA_REAL_MOTOR
#define CANNULA_VISUALIZATION_REAL_CANNULA_REAL_MOTOR
#include "interfaces/abstract_motor_interface.h"

// Class that interacts with the real motors. Unlike the MotorSimulation, the 
// RealMotor will be able to apply motor speeds, advance motor positions, and
// retrieve the position of the real motor instead of a simulated one. We can 
// use separate motor IDs to denote different hardware motors that are present
// in the Cannula hardware platform. This way, each instance of the RealMotor
// class should correspond to a single real motor used in the Cannula platform.
class RealMotor : public AbstractMotorInterface {
  virtual void set_motor_speed(double motor_speed) { };
  virtual void set_goal_position(double new_goal_position) { };

  virtual void set_position(double position) { };

  virtual void go_distance(double delta_distance) { };

  /*
  * Accessors
  */
  virtual double position() const { };
  virtual double goal_position() const { };

  virtual double max_motor_speed() const { };
  virtual double motor_speed() const { };

  virtual double get_distance_given_elapsed_time(double milliseconds) const { };
  virtual bool is_goal_position_reached() const { };
};

#endif // CANNULA_VISUALIZATION_REAL_CANNULA_REAL_MOTOR