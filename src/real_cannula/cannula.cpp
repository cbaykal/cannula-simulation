#include "real_cannula/cannula.h"
#include "real_cannula/real_motor.h"

RealCannula::Cannula::Cannula()
    : is_robot_running_(false), 
      have_set_motor_distance_goals_(false),
      asked_for_goal_state_(false),
      goal_state_(0),
      motors_per_tube_(0) {
  // initalize thread
  moveToThread(this);
  // explicity set the elapsed time; calculating it
  // directly was causing nonlinear behavior of the motors
  // elapsed time is now in member variable elapsed_time_milliseconds_
  motor_update_interval_milliseconds_ =
      size_t(kMillisecondsPerSecond/kMotorUpdatesPerSecond);
      
}

RealCannula::Cannula::~Cannula() {
  turn_off_robot();
  // Free all of the motors in the vector. (VC10 doesn't support range based 
  // loops yay).
  for (auto it = motors_vector_.begin(); it != motors_vector_.end(); ++it) {
    delete (*it);
  }

  quit();
  wait();
}

void RealCannula::Cannula::run() {
  motors_update_timer_ = std::make_shared<QTimer>(this);
  motors_update_timer_->moveToThread(this);
  connect(motors_update_timer_.get(), SIGNAL(timeout()), this, SLOT(update_motor_states()));
  motors_update_timer_->start(int(kMillisecondsPerSecond/kMotorUpdatesPerSecond));
  exec();
}

void RealCannula::Cannula::add_motor(double max_motor_speed) {
  // Add a real_motor here instead of a simulated motor.
  
}

/*********************
  There's a lot of code duplication going on in here (with simulation.cpp). I 
  think the best way to handle this would be to put functions that contain common 
  functionality to AbstractCannulaInterface. This would not make the interface
  abstract anymore. Then, in simulation.cpp, we can call the parent's version of
  the method as well as invoke methods to notify the visualization thread with
  a new shape to render.
**********************/
void RealCannula::Cannula::update_motor_states() {
  // We need a new goal state, so notify the configurations queue manager
  if (!goal_state_.initialized_information() || goal_state_.state_reached()) {
    if (!asked_for_goal_state_) {
      emit need_goal_configuration();
      // this flag makes sure that we only ask for 
      // the goal once per udpate need
      asked_for_goal_state_ = true;
    }

    return;
  }

  if (!have_set_motor_distance_goals_) {
    set_motor_goal_positions();
    
  } else if (are_all_position_goals_reached()) {
    // we have reached the goal state, so
    // reflect this on the current goal state
    goal_state_.set_state_reached(true);
  } else {
    // if we are here, the motors have been set distance goals,
    // but they haved not reached them yet, so advance distances
    advance_motor_positions(motor_update_interval_milliseconds_);
  }
}

// TODO: Check hardware positions of the real motors to determine whether the 
// goal positions are reached.
bool RealCannula::Cannula::are_all_position_goals_reached() const {

  return true;
}

// The equivalent function to this one in simulation.cpp (simulated robot code)
// first finds the maximum time required to reach the goal and then sets the 
// motor distances accordingly.
void RealCannula::Cannula::set_motor_goal_positions() {

}

void RealCannula::Cannula::advance_motor_positions(
                            double elapsed_time_milliseconds) {
}

// This function is called whenever the configurations queue sends to the 
// Composite Cannula a new goal configuration that was dequedf from the front. 
// However, the new goal configuration signal emitted by the queue is a result 
// of the  *simulated* cannula needing a new goal configuration because it has 
// reached its prior goal configuration already.
void RealCannula::Cannula::new_goal_configuration(StateInformation goal_state) {
  // Code duplication again with simulation.cpp
  goal_state_ = goal_state;
  goal_state_.set_initialized_information(true);
  asked_for_goal_state_ = have_set_motor_distance_goals_ = false;
}

void RealCannula::Cannula::print_current_state() const {
}