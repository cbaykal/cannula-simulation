#include "visualization/dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::keyPressEvent(QKeyEvent *e){ 
	if (e->key() == Qt::Key_R) {
		ui->widget->setRecording(!ui->widget->getRecording());
	} else if (e->key() == Qt::Key_P) {
		ui->widget->setPlayingBack(!ui->widget->getPlayingBack());
	} else if (e->key() == Qt::Key_M) {
    ui->widget->set_is_motion_planning(!ui->widget->is_motion_planning());
  }
}




