#include "visualization/glwidget.h"
#include "visualization/dialog.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

//#define PRETTY_MESH
#define DISPLAY_CANNULA

GLWidget::GLWidget(QWidget *parent)
    : QGLWidget(QGLFormat(), parent),
      // TODO: Read these from a file using Boost.
      kVisualizationSettingsFileName("visualization_settings.xml"),
      root_("plugins.cfg"),
      timer(this),
      isMousePressed(false),
      is_motion_planning_(true),
      createdCannula(false),
      createdScene(false),
      recording(false), 
      playingBack(false) {
  // Resize the screen to fit given height.
	resize(kWidth, kHeight);
  initialize_visualization_settings();
  // Load required resources for Ogre (materials, meshes, etc.)
	loadResources();
	// Configure Ogre
	configure();

	// Initialize the MeshManager which will be used to create a mesh of the 
  // Cannula dynamically for display.
	std::cout << "Initializing Mesh Manager..." << std::endl;
	initializeMeshManager();

	// Initialize the Cannula and CannulaPlanning pointers and load the roadmap.
	initializeCannulaPlanner();
  std::cout << "Initialized Cannula Planner" << std::endl;
	std::cout <<"Initializing Haptic Device..." << std::endl;

  initializeCannulaSimulation();
  std::cout << "Initialized Cannula Simulation" << std::endl;
	initializeHapticDevice();

	// Utilize a QTimer for updating the visualization at a defined FPS.
	connect(&timer, SIGNAL(timeout()), this, SLOT(update()));
	timer.start(kMillisecondsPerSecond/kFps);
  std::cout << "Started timer" << std::endl;
}

// Ogre::root is deallocated automatically. This deallocation of the root 
// object is responsible for deallocating all of the pointers to Ogre objects.
// Thus, there is no need to deallocate any of those pointers here.
GLWidget::~GLWidget() {
	// Clean up rendering context
	hlMakeCurrent(NULL);

	if (hHLRC != NULL) {
		hlDeleteContext(hHLRC);
	}

	// Free up haptic device
	if (hHD != HD_INVALID_HANDLE) {
		hdDisableDevice(hHD);
	}

  composite_cannula_.turn_off_robot();
}

// Phantom callback function: sets the position variable with Phantom's location
HDCallbackCode HDCALLBACK stateCallback(void *pUserData) {
	DeviceState *deviceState = static_cast<DeviceState *>(pUserData);
	// populate position vector
	hdGetDoublev(HD_CURRENT_POSITION, deviceState->position);
	// populate orientation vector
	hdGetDoublev(HD_CURRENT_GIMBAL_ANGLES, deviceState->orientation);

	// Figure out whether the button was pressed or released
	HDint currentButtons, lastButtons;

	// retrieve the values from the Phantom
	hdGetIntegerv(HD_CURRENT_BUTTONS, &currentButtons);
  hdGetIntegerv(HD_LAST_BUTTONS, &lastButtons);

	// Check to see whether the button was pressed
    if ((currentButtons & HD_DEVICE_BUTTON_1) != 0 &&
        (lastButtons & HD_DEVICE_BUTTON_1) == 0) {
		deviceState->isButtonPressed = !deviceState->isButtonPressed;
	} 

	return HD_CALLBACK_CONTINUE;
}

// initialize the Phantom device so that it is ready for use
void GLWidget::initializeHapticDevice() {
	HDErrorInfo error;

	hHD = hdInitDevice(HD_DEFAULT_DEVICE);

	if (HD_DEVICE_ERROR(error = hdGetError())) {
		QMessageBox::critical(this, tr("Error"), 
                          tr("Failed to initialize haptic device."), 0, 0);
	}

	hdEnable(HD_FORCE_OUTPUT);
	// initialize context for HL functions
	hHLRC = hlCreateContext(hHD);
	hlMakeCurrent(hHLRC);

	// Start the haptic rendering loop.
  hdStartScheduler();

	// initialize callback loop in order to retrieve and store the state of the Phantom device
	stateCallbackHandle = hdScheduleAsynchronous(stateCallback,
                                               &deviceState,
                                               HD_MAX_SCHEDULER_PRIORITY);
}

// Sets the string member variable, simulation_settings_file_name_, by reading
// in the settings file name that should be used 
void GLWidget::read_simulation_settings_file_name() {
  std::cout << "Please enter the name of the XML file containing the settings"
               "for this application.";
  std::cin >> simulation_settings_file_name_;

  if (simulation_settings_file_name_.find(".xml") == std::string::npos) {
    simulation_settings_file_name_.append(".xml");
  }
}

// Reads the XML file named visualization_settings.xml in order to populate
// a map of settings that include the names of required files for the Cannula
// and options for the simulation. Additionally, populates the vector of Motors
// contained by the simulation by adding motors with given motor speeds.
void GLWidget::initialize_visualization_settings() {
  boost::property_tree::ptree pt;

  try {
    boost::property_tree::read_xml(
          kVisualizationSettingsFileName,
          pt,
          boost::property_tree::xml_parser::trim_whitespace);

    // Step through different options and add them to the map.
    simulation_settings_.insert(
        std::make_pair("problem_name", pt.get<std::string>("visualization.problem_name")));

    std::cout << "problem_name" << pt.get<std::string>("visualization.problem_name") << std::endl;


    simulation_settings_.insert(
        std::make_pair("roadmap_name", pt.get<std::string>("visualization.roadmap_name")));
    std::cout << "roadmap_name" << pt.get<std::string>("visualization.roadmap_name") << std::endl;

    simulation_settings_.insert(std::make_pair("environment_name",
                                pt.get<std::string>("visualization.environment_name")));

    simulation_settings_.insert(
        std::make_pair("log_name", pt.get<std::string>("visualization.log_name")));    

    simulation_settings_.insert(std::make_pair("motors_per_tube",
                                pt.get<std::string>("visualization.simulation.motors_per_tube")));

    // Iterate through the nested structure and get the  max speeds for alpha &
    // beta for each motor
    BOOST_FOREACH(const boost::property_tree::ptree::value_type &motor,
                  pt.get_child("visualization.simulation.motors")) {
      simulated_cannula_.add_motor(motor.second.get<double>("max_speed"));
    } // end BOOST_FOREACH
  } catch (std::exception &e) {
    throw std::runtime_error(e.what());

  } catch (...) {
    std::cout << "Exception from initialize_visualization_settings" << std::endl;
    throw;
  }
}

void GLWidget::initializeCannulaPlanner() {
	// Open the input XML file and read in the cannula's device parameters and
  // the configuration we want to simulate
	try {
    auto input_file_name_it = simulation_settings_.find("problem_name");
    if (input_file_name_it == simulation_settings_.end()) {
      throw std::runtime_error(
                "The filename containg the Cannula problem is not set.");
    }

		boost::property_tree::ptree pt;
    const std::string file_name = (input_file_name_it->second);
    std::cout << "File name is: " << file_name << std::endl;
		boost::property_tree::read_xml(file_name, pt);

    // TODO: Only need one pointer to the cannula in the new version of the
    // planning code.
    // Using smart pointers here because loadCannula explicitly states that the 
    // callee is responsible for deallocating memory pointed to by the returned
    // Cannula pointer.

    // Transfer ownership of the Cannula pointer to our member shared_ptr.
		cannula_for_simulation_ =  
        std::shared_ptr<Cannula>(
            CannulaUtils::loadCannula(pt.get_child("cannula_problem.cannula")));

    // TODO: This should no longer be needed.
    // Transfer ownership of the second Cannula pointer to our member shared_ptr.
    cannula_for_planning_=
        std::shared_ptr<Cannula>(
            CannulaUtils::loadCannula(pt.get_child("cannula_problem.cannula")));

		bool withoutTorsion = pt.get<bool>("cannula_problem.without_torsion", false);
		double stepSize = pt.get<double>("cannula_problem.step_size");

    auto environment_file_name_it = simulation_settings_.find("environment_name");

    if (environment_file_name_it == simulation_settings_.end()) {
      throw std::runtime_error(
          "File name containing description of the environment is not set.");
    }

		env.load(environment_file_name_it->second);

		cannula_planning_ = 
        std::shared_ptr<CannulaPlanning>(
            new CannulaPlanning(*cannula_for_planning_, env, stepSize, 5));
    
    auto roadmap_name_it = simulation_settings_.find("roadmap_name");

    if (roadmap_name_it == simulation_settings_.end()) {
      throw std::runtime_error("File name containing roadmap name is not set.");
    }

		std::cout << "Loading roadmap: " << roadmap_name_it->second << "...\n";
		boost::timer::cpu_timer timer;

		cannula_planning_->loadRoadmap(roadmap_name_it->second);
		timer.stop();

		std::cout << "Done loading " << roadmap_name_it->second << ".\n";
		std::cout << "Loading took " << timer.format(4, "%w") << " seconds.\n";

	} catch (std::exception &e) {
		std::cout << "Exception: " << e.what() << std::endl;
		QMessageBox::warning(
        this->parentWidget(), tr("Exception"), QString(e.what()), 0, 0);
	}
}

void GLWidget::initializeCannulaSimulationConnections() {
  // simulation will emit a signal whenever a shape is ready to be rendered
  connect(&simulated_cannula_,
          SIGNAL(shape_ready(const std::vector<Eigen::Vector3d> *,
                             const std::vector<Eigen::Vector3d> *,
                             const std::vector<Eigen::Matrix<int, 6, 1>>*,
                             const Eigen::VectorXi *,
                             const Eigen::VectorXi *)),
          this,
          SLOT(draw_shape(const std::vector<Eigen::Vector3d> *,
                          const std::vector<Eigen::Vector3d> *,
                          const std::vector<Eigen::Matrix<int, 6, 1>>*,
                          const Eigen::VectorXi *,
                          const Eigen::VectorXi *)),
          Qt::QueuedConnection);
  
  // the visualization thread (this thread) should emit
  // a signal whenever we have a new point to plan for
  connect(this,
          SIGNAL(new_goal_point(Eigen::Vector3d*)),
          &simulation_planner_,
          SLOT(update_motion_plan_for_new_goal(Eigen::Vector3d*)),  
          Qt::QueuedConnection);
  
  connect(&simulation_queue_manager_,
          SIGNAL(set_planning(bool)),
          this,
          SLOT(set_planning_slot(bool)));
  
  // whenever the planner has a new plan
  // it should let the configurations queue
  // manager know of the new plan
  connect(&simulation_planner_,
        SIGNAL(notify_configurations_queue_manager(std::deque<StateInformation*>*)),
        &simulation_queue_manager_,
        SLOT(update_queue_with_plan(std::deque<StateInformation*>*)),
        Qt::QueuedConnection);
          
  // if the robot simulation needs a goal configuration,
  // let the configurations queue manager know
  connect(&simulated_cannula_,
          SIGNAL(need_goal_configuration()),
          &simulation_queue_manager_,
          SLOT(get_next_configuration()),
          Qt::QueuedConnection);
  
  // In turn, the configurations queue should
  // send the new goal configuration to the
  // robot simulation as necessary
  connect(&simulation_queue_manager_,
          SIGNAL(send_next_configuration(StateInformation)),
          &composite_cannula_,
          SLOT(new_goal_configuration(StateInformation)),
          Qt::QueuedConnection); // Qt::QueuedConnection
}

void GLWidget::initializeCannulaSimulation() {
  auto motors_per_tube_it = 
      simulation_settings_.find("motors_per_tube");

  try {
    if (motors_per_tube_it == simulation_settings_.end()) {
      throw std::runtime_error("Motors per tube is not set.");
    }
  } catch (std::exception &e) {
    std::cout << e.what() << std::endl;
    throw;
  }

  simulated_cannula_.set_motors_per_tube(std::stoi(motors_per_tube_it->second));
  // robot simulation (in charge of the simulated motors) needs to know of the 
  // cannula for initial minimum betas.
  simulated_cannula_.set_cannula(cannula_for_simulation_);

  // Planner needs to know of the robot simulation to set initial minimum beta
  // values.
  simulation_planner_.set_simulated_cannula(&simulated_cannula_);

  // simulation planner needs to know of the CannulaPlanning variable planner
  simulation_planner_.set_cannula_planning(cannula_planning_);
  simulation_planner_.set_num_tubes(cannula_for_planning_->getNumTubes());

  initializeCannulaSimulationConnections();

  real_cannula_.print_current_state();
  composite_cannula_.add_cannula_interface(&real_cannula_);
  composite_cannula_.add_cannula_interface(&simulated_cannula_);

  simulation_planner_.start();
  simulation_queue_manager_.start();
  composite_cannula_.turn_on_robot();
}

// configure Ogre Application settings
void GLWidget::configure() {
	Ogre::RenderSystem *renderSystem;
	renderSystem = root_.getAvailableRenderers().at(0);
	root_.setRenderSystem(renderSystem);

	// set the height and width to the height and width of the widget
	QString dimensions = QString("%1x%2")
							         .arg(this->width())
							         .arg(this->height());

	// toStdString() is buggy, use dimensions.toUtf8() instead
	renderSystem->setConfigOption("Video Mode", 
                                dimensions.toUtf8().constData());

	root_.getRenderSystem()->setConfigOption("Full Screen", "No");

	// don't create a window since we'll be using the the widget instead
	root_.initialise(false); 
}

void GLWidget::loadResources() {
	// Load resource paths from config file
	Ogre::ConfigFile cf;
	cf.load("resources.cfg");

	// Go through all sections and settings in the file
	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

	Ogre::String secName, typeName, archName;
	while (seci.hasMoreElements()) {
		secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for (i = settings->begin(); i != settings->end(); ++i) {
			typeName = i->first;
			archName = i->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
		}
	}
}

void GLWidget::createCamera() {
	camera_ = sceneManager->createCamera("MainCamera");
	camera_->setFixedYawAxis(true, Ogre::Vector3::UNIT_Z);
	/*camera->setFixedYawAxis(true, Ogre::Vector3(0.0, -0.6478959344, 0.76175998));*/
	camera_->setPosition(Ogre::Vector3(0.0, -0.1, 0.0));
	// look at the origin (where we will display the mesh)
	camera_->lookAt(0, 0, 0);
	
	// clip distances
	camera_->setNearClipDistance(0.01);
	camera_->setFOVy(Ogre::Degree(60));
}

void GLWidget::createViewport() {
	viewport_ = renderWindow->addViewport(camera_);
	viewport_->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0, 1.0));
}

// TODO: Function way too long; split it up into subfunctions.
void GLWidget::createScene() {
	// set shadow technique (use stencil shadows because they are the easiest to use at the moment; texture shadows require shaders)
	sceneManager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
	sceneManager->setShadowColour(Ogre::ColourValue::Black);

	createCamera();
	createViewport();
	
	// Display the environment mesh
	try {
#ifdef PRETTY_MESH
		Ogre::Entity* environmentEntity = sceneManager->createEntity("environment", "icra_textured_mesh.mesh");
#else
		Ogre::Entity* environmentEntity = sceneManager->createEntity("environment", "icra_collisions.mesh");
		Ogre::MaterialManager::getSingleton()
        .create("BoneMaterial", 
                Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		environmentEntity->setMaterialName("BoneMaterial");
#endif
		Ogre::SceneNode *environmentSceneNode = 
        sceneManager->getRootSceneNode()->createChildSceneNode();
		// Enabling shadows on the environment entity causes it to be displayed in a strange way; do we need shadows?
		environmentEntity->setCastShadows(false);
		environmentSceneNode->attachObject(environmentEntity);

	} catch (Ogre::Exception& e) {
		std::cout << "Exception: " << e.getFullDescription() << std::endl;
		QMessageBox::warning(this->parentWidget(), tr("Exception"), QString::fromLocal8Bit((e.getFullDescription().c_str())), 0, 0);
	}	

	float ambientLight = .40;
	sceneManager->setAmbientLight(Ogre::ColourValue(ambientLight, ambientLight, ambientLight, 1.0));

	// add a point light wherever the sphere is
	cannulaLight= sceneManager->createLight();
	cannulaLight->setType(Ogre::Light::LT_POINT);
	cannulaLight->setDiffuseColour(1.0, 1.0, 1.0);

	// directional light
	Ogre::Light* directionalLight = sceneManager->createLight();
	directionalLight->setType(Ogre::Light::LT_DIRECTIONAL);
	directionalLight->setDirection(0, 1, -1);
  float directionalLightColor[3] = {1.0, 1.0, 1.0};
	directionalLight->setDiffuseColour(directionalLightColor[0], directionalLightColor[1], directionalLightColor[2]);

#ifdef PRETTY_MESH
	Ogre::Light* directionalLightSide = sceneManager->createLight();
	directionalLightSide->setType(Ogre::Light::LT_DIRECTIONAL);
	directionalLightSide->setDirection(-1, 1, -1);
	directionalLightSide->setDiffuseColour(directionalLightColor[0], directionalLightColor[1], directionalLightColor[2]);

	Ogre::Light* directionalLightSideTwo = sceneManager->createLight();
	directionalLightSideTwo->setType(Ogre::Light::LT_DIRECTIONAL);
	directionalLightSideTwo->setDirection(1, 1, -1);
	directionalLightSideTwo->setDiffuseColour(directionalLightColor[0], directionalLightColor[1], directionalLightColor[2]);
#endif

	// create a SceneNode to attach the Cannula Entity to
	cannula_scene_node_ = 
          sceneManager->getRootSceneNode()->createChildSceneNode();
	cannula_scene_node_->setPosition(0, 0, 0);

#ifdef DISPLAY_CANNULA
	// now add the cylinder to the bottom of the cannula
	Ogre::Entity* cylinderEntity = 
      sceneManager->createEntity("cylinder", "cylinder.mesh");
	cannula_scene_node_->attachObject(cylinderEntity);

	Ogre::MaterialManager::getSingleton().create("CylinderMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	cylinderEntity->setMaterialName("CylinderMaterial");
#endif

	// load material for the Cannula (i.e. colors for the Cannula)
	Ogre::MaterialPtr cannulaMaterial = Ogre::MaterialManager::getSingleton().create("CannulaMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	cannulaMaterial->getTechnique(0)->getPass(0)->setVertexColourTracking(Ogre::TVC_DIFFUSE);
	cannulaMaterial->getTechnique(0)->getPass(0)->setDiffuse(Ogre::ColourValue(1.0, 1.0, 1.0));
	cannulaMaterial->getTechnique(0)->getPass(0)->setLightingEnabled(true);

	initializeSphere(.004);
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
	if (event->button() == Qt::MouseButton::RightButton) {
		set_is_motion_planning(true);
	} else if (event->button() == Qt::MouseButton::LeftButton) {
		// stop planning while we are adjusting the camera
    set_is_motion_planning(false);
		isMousePressed = false;
		// set the start point
		startPoint = event->pos();
	}

	QGLWidget::mousePressEvent(event);
}

void GLWidget::mouseReleaseEvent(QMouseEvent *event) {
	isMousePressed = false;
	QGLWidget::mouseReleaseEvent(event);
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
	// if the mouse is pressed down and we are moving, adjust camera_ accordingly
	if (isMousePressed) {
		// construct a relative point which will contain the deltas between the x and y coordinates of the start and ending points
		Ogre::Real dist = (camera_->getPosition() - cannula_scene_node_->getPosition()).length();
		QPoint relativePoint = event->pos() - startPoint;
		startPoint = event->pos();

		camera_->setPosition(Ogre::Vector3(0, 0, 0));

		float multiplier = 2.0;

		camera_->yaw(Ogre::Degree(-relativePoint.x() * multiplier));
		camera_->pitch(Ogre::Degree(-relativePoint.y() * multiplier));

		// TODO: Make it so that the Phantom coordinates are used here
		camera_->moveRelative(Ogre::Vector3(0, 0, dist));
	}

	QGLWidget::mouseMoveEvent(event);
}

void GLWidget::initializeGL() {
	Ogre::String winHandle;

	winHandle += Ogre::StringConverter::toString((unsigned long)(this->parentWidget()->winId()));

	Ogre::NameValuePairList params;
	params["parentWindowHandle"] = winHandle;
	renderWindow = root_.createRenderWindow("Ogre_RenderWindow",
											                    this->width(),
											                    this->height(),
											                    false,
											                    &params);

	renderWindow->setActive(true);
	WId ogreWinId = 0x0;
	renderWindow->getCustomAttribute("WINDOW", &ogreWinId);

	assert(ogreWinId);

	QRect geo = this->frameGeometry();
	this->create(ogreWinId);
	this->setGeometry(geo);

	setAttribute(Qt::WA_PaintOnScreen);
	setAttribute(Qt::WA_NoBackground);
	sceneManager = root_.createSceneManager(Ogre::ST_EXTERIOR_CLOSE);
	// initialize all resource groups
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

void GLWidget::resizeGL(int width, int height) {
	renderWindow->reposition(this->pos().x(), this->pos().y());    
	renderWindow->resize(width, height);
	paintGL();
}

void GLWidget::paintGL() {
	// create the scene if it is not created already
	if (!createdScene) {
		createScene();
		createdScene = true;
	}

	try {
		// if the mesh for the Cannula is created, create an Entity
		if (createdCannula) {
#ifdef DISPLAY_CANNULA
			// destroy the CannulaEntity so that we don't end up creating another Entity with the same name
			if (sceneManager->hasEntity("CannulaEntity")) {
				cannula_scene_node_->detachObject("CannulaEntity");
				sceneManager->destroyEntity("CannulaEntity");
			}
			
			Ogre::Entity* cannulaEntity = sceneManager->createEntity("CannulaEntity", "CannulaMesh");
			cannula_scene_node_->attachObject(cannulaEntity);
			cannulaEntity->setMaterialName("CannulaMaterial");
			cannulaEntity->setCastShadows(true);
#endif
		}

		// Finally, render one frame.
		root_.renderOneFrame();

	} catch (Ogre::Exception& e) {
		std::cout << "Exception: " << e.getFullDescription() << std::endl;
		QMessageBox::warning(this->parentWidget(), tr("Exception"), QString::fromLocal8Bit((e.getFullDescription().c_str())), 0, 0);
	}	
}

void GLWidget::initializeMeshManager() {
	 /*Create the mesh via the MeshManager*/
	try {
		mesh_ = Ogre::MeshManager::getSingleton()
               .createManual("CannulaMesh", "General");
		cannula_mesh_ = mesh_->createSubMesh();

	} catch (Ogre::Exception&e) {
		std::cout << "Exception: " << e.getFullDescription() << std::endl;
		QMessageBox::warning(this->parentWidget(), tr("Exception"), QString::fromLocal8Bit((e.getFullDescription().c_str())), 0, 0);
	}
}

// create the mesh that will be displayed
void GLWidget::createCannulaMesh(std::vector<Eigen::Vector3d> vertices,
                                 std::vector<Eigen::Vector3d> normals,
                                 std::vector<Eigen::Matrix<int, 6, 1>> faces,
                                 Eigen::VectorXi tubeStarts,
                                 Eigen::VectorXi tubeEnds) {
  using namespace Ogre;
	const size_t numVertices = vertices.size();
	// (3 floats for position + 3 floats for normal)*nVertices = 3*2*nVertices
  const size_t bufferCount = 3*2*vertices.size();
	float* verticesBuffer = new float[bufferCount];

  RenderSystem* rs = Root::getSingleton().getRenderSystem();
  RGBA *colours = new RGBA[numVertices];
	// pointer to the very first element of the array (will use this to incrementally set the colors of the vertices)
	RGBA *pColour = colours;
	
	unsigned vertexIndex = 0;
	
  // normalize normals
	for (auto it = normals.begin(); it != normals.end(); ++it) {
		it->normalize();
  }

	for (unsigned i = 0; i < vertices.size(); ++i) {
		for (unsigned j = 0; j < 3; ++j) {
			verticesBuffer[vertexIndex++] = vertices[i](j);
		}

		for (unsigned j = 0; j < 3; ++j) {
			verticesBuffer[vertexIndex++] = normals[i](j);
		}
	}

	float alpha = 1.0;
	float colors[4][4] = {{0.0f, 1.0f, 1.0f, alpha},
				{1.0f, 0.65f, 0.0f, alpha},
				{1.0f, 0, 1.0f, alpha},
				{1.0f, 1.0f, 0.0f, alpha}};

	size_t numColors = 4;

	// iterate through the different tubes and color the vertices accordingly
	for (unsigned tubeIdx = 0; tubeIdx < tubeStarts.size(); ++tubeIdx) {
		// pick the right color
		unsigned color = tubeIdx % numColors;

		for (int faceIdx = tubeStarts(tubeIdx); faceIdx <= tubeEnds(tubeIdx); ++faceIdx) {
			for (unsigned i = 0; i < 3; ++i) {
				rs->convertColourValue(ColourValue(colors[color][0], colors[color][1], colors[color][2], colors[color][3]), 
									&colours[faces[faceIdx](i)]);
			} 
		} // end for faces
	} // end for tubes

	// Define the index buffer used to correctly connect the vertices to form faces
  const size_t indicesBufferSize = faces.size()*3;
	
  unsigned short *indicesBuffer = new unsigned short[indicesBufferSize];

	for (unsigned faceIdx = 0; faceIdx < faces.size(); ++faceIdx) {
		for (unsigned i = 0; i < 3; ++i) {
			indicesBuffer[faceIdx*3 + i] =  faces[faceIdx](i);
		}
	}

    // Create vertex data structure for the required number of vertices to hold data
    mesh_->sharedVertexData = new VertexData();
    mesh_->sharedVertexData->vertexCount = numVertices;
    // Create declaration (memory format) of vertex data
    VertexDeclaration* decl = mesh_->sharedVertexData->vertexDeclaration;
    size_t offset = 0;
    // 1st buffer
    decl->addElement(0, offset, VET_FLOAT3, VES_POSITION);
    offset += VertexElement::getTypeSize(VET_FLOAT3);
    decl->addElement(0, offset, VET_FLOAT3, VES_NORMAL);
    offset += VertexElement::getTypeSize(VET_FLOAT3);

    // Allocate vertex buffer of the requested number of vertices (vertexCount) 
    // and bytes per vertex (offset)
    HardwareVertexBufferSharedPtr vbuf = HardwareBufferManager::getSingleton()
										  .createVertexBuffer(offset, mesh_->sharedVertexData->vertexCount, HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
    // Upload the vertex data to the card
    vbuf->writeData(0, vbuf->getSizeInBytes(), verticesBuffer, true);
 
    // Set vertex buffer binding so buffer 0 is bound to our vertex buffer
    VertexBufferBinding* bind = mesh_->sharedVertexData->vertexBufferBinding; 
    bind->setBinding(0, vbuf);
 
    // 2nd buffer
    offset = 0;
    decl->addElement(1, offset, VET_COLOUR, VES_DIFFUSE);
    offset += VertexElement::getTypeSize(VET_COLOUR);
    // Allocate vertex buffer of the requested number of vertices (vertexCount) 
    // and bytes per vertex (offset)
    vbuf = HardwareBufferManager::getSingleton().createVertexBuffer(
            offset, mesh_->sharedVertexData->vertexCount,
            HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
    /// Upload the vertex data to the card
    vbuf->writeData(0, vbuf->getSizeInBytes(), colours, true);
 
  // Set vertex buffer binding so buffer 1 is bound to our colour buffer
    bind->setBinding(1, vbuf);
 
  // Allocate index buffer of the requested number of vertices (ibufCount) 
    HardwareIndexBufferSharedPtr ibuf =
         HardwareBufferManager::getSingleton()
						.createIndexBuffer(HardwareIndexBuffer::IT_16BIT, indicesBufferSize,
                                HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
 
  // Upload the index data to the card
  ibuf->writeData(0, ibuf->getSizeInBytes(), indicesBuffer, true);
 
  // Set parameters of the submesh
  cannula_mesh_->useSharedVertices= true;
  cannula_mesh_->indexData->indexBuffer = ibuf;
  cannula_mesh_->indexData->indexCount = indicesBufferSize;
  cannula_mesh_->indexData->indexStart = 0;
	
  // Set bounding information (for culling)
  mesh_->_setBounds(AxisAlignedBox(-100,-100,-100,100,100,100));
  mesh_->_setBoundingSphereRadius(Math::Sqrt(3*100*100));
	
  mesh_->freeEdgeList();
  // Build edge list (for shadows)
  mesh_->sharedVertexData->prepareForShadowVolume();
  mesh_->buildEdgeList();

  // Notify Mesh object that it has been loaded
  mesh_->load();

  createdCannula = true;
  
  // free memory
  delete []verticesBuffer;
  delete []indicesBuffer;
  delete []colours;
}

// function for initializing the sphere which will be used to display the transformed position of the haptic device
void GLWidget::initializeSphere(float radius) {
	Ogre::Entity* sphereEntity = sceneManager->createEntity("HapticSphere", Ogre::SceneManager::PT_SPHERE);
	
	// store the pointer to the sphereSceneNode in a class variable so that we can use it to position the sphere
	sphere_scene_node_ = sceneManager->getRootSceneNode()->createChildSceneNode();
	sphere_scene_node_->attachObject(sphereEntity);

	// set radius and set SphereMaterial, which is defined in a .material script
	sphere_scene_node_->setScale(radius/50, radius/50, radius/50);

	Ogre::MaterialManager::getSingleton().create("SphereMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	sphereEntity->setMaterialName("SphereMaterial");
	sphereEntity->setCastShadows(true);
}

Eigen::Vector3d GLWidget::getGoalConfiguration() {
	if(getPlayingBack() && !configurationQueue.empty()) {
    // if we are playing the recording back, just return the next configuration in the queue
		Eigen::Vector3d retVec = configurationQueue.front();
		configurationQueue.pop();
		return retVec;

	} else if(getPlayingBack() && configurationQueue.empty()) {
		setPlayingBack(false);
	}

	// store the "raw" position of the Phantom device in a column vector (positions defined in millimeters)
	Eigen::Vector4d positionVector(deviceState.position[0],
                                 deviceState.position[1], deviceState.position[2], 1);

	// TODO: Read these from file.
	const double scalingFactorX = .002, // .0008
	             scalingFactorY = .002, // .0008
	             scalingFactorZ = .002; // .0008

	Ogre::Matrix3 ogreViewMatrix;
	camera_->getViewMatrix().extract3x3Matrix(ogreViewMatrix);

	Eigen::Matrix4d rotationMatrix;

	// populate the 3x3 rotation matrix of the transformation matrix
	for (size_t r = 0; r < 3; ++r) {
		for (size_t c = 0; c < 3; ++c) {
			// apply the appropriate scaling depending on whether we are at the diagonal
			rotationMatrix(r, c) = ogreViewMatrix[c][r];
		}
	}

	// initialize scaling matrix
	Eigen::Matrix4d scalingMatrix;
	scalingMatrix << scalingFactorX, 0, 0, 0,
					         0, scalingFactorY, 0, 0,
					         0, 0, scalingFactorZ, 0,
					         0, 0, 0, 1;
	
	// populate the last column
	rotationMatrix.col(3) << 0, 0, 0, 1;
	// populate the last row of the matrix
	rotationMatrix.row(3) << 0, 0, 0, 1;

	// define translation matrix
	Eigen::Matrix4d translationMatrix;
	// apply a translation so that (0, 0, 0) is the respective (x, y, z) coordinates for the Phantom
	translationMatrix << 1, 0, 0, 0,
					             0, 1, 0, .05,
					             0, 0, 1, .05,
						           0, 0, 0, 1;

	// compute the result
	Eigen::Vector4d resultantVector4d = scalingMatrix * rotationMatrix * translationMatrix * positionVector;

  // Vector3d version of the result (without w)
	Eigen::Vector3d resultantVector3d(resultantVector4d[0], resultantVector4d[1], resultantVector4d[2]);

	// if we are recording this information, write to file
	if (getRecording()) {
		// get the elapsed time since the user has begun recording in milliseconds
		auto nanoseconds = boost::chrono::nanoseconds(recordingTimer.elapsed().user + recordingTimer.elapsed().system);
		auto milliseconds = boost::chrono::duration_cast<boost::chrono::milliseconds>(nanoseconds);
	
		// write the *transformed* coordinates of the phantom to the file
		for (size_t i = 0; i < 3; ++i) {
			record_file_ << resultantVector3d(i) << " ";
		}

		// add in the milliseconds as a last argument
		record_file_ << milliseconds.count() << std::endl;
	}

	// return the goal configuration to the transformed haptic coordinates
	return resultantVector3d;
}

void GLWidget::positionSphere(const Eigen::Vector3d& goalConfiguration) const {
	sphere_scene_node_->setPosition(goalConfiguration[0], goalConfiguration[1], goalConfiguration[2]);
}

// update at specified FPS
void GLWidget::update() {
	if (myStartPoint.getX() == 0 && myStartPoint.getY() == 0 && myStartPoint.getZ() == 0) {
		myStartPoint.setXYZ(deviceState.position[0], deviceState.position[1], deviceState.position[2]);
    return;
	}
	  
  // update previous and current points for camera positioning purposes
	StoragePoint currentPoint(deviceState.position[0], deviceState.position[1], deviceState.position[2]);
	StoragePoint relativePoint = currentPoint - myStartPoint;
	myStartPoint = currentPoint;

	// if the Phantom's button is pressed, then adjust the camera
  // if not, use the coordinates to get an updated goal point
	if (!deviceState.isButtonPressed) {
		// get the goal configuration for the Cannula
    Eigen::Vector3d goalConfiguration = getGoalConfiguration();
    // memory will be copied by the planner thread
    Eigen::Vector3d *copy_goal_configuration = new Eigen::Vector3d;
    
#ifdef PRETTY_MESH
		// place a light at the goal configuration to make the Cannula easier to see inside the skull
		cannulaLight->setPosition(goalConfiguration[0], goalConfiguration[1], goalConfiguration[2]);
#endif  
		// position the sphere to correspond to the goal configuration that the user wants the Cannula to reach
		positionSphere(goalConfiguration);
		// new "simulation" code to portray realism, rather than instantenous rendering of the shape of the cannula at the goal configuration
		if (is_motion_planning()) {
      // we have a new goal point to plan for, so let the simulation
      // planner know that we need a new plan for this point
      *copy_goal_configuration = goalConfiguration;
      emit new_goal_point(copy_goal_configuration);
		}
		
	} else {
		// construct a relative point which will contain the deltas between the x and y coordinates of the start and ending points
		Ogre::Real dist = 
      (camera_->getPosition() - cannula_scene_node_->getPosition()).length();

    // set the position of the camera relative to 
		camera_->setPosition(Ogre::Vector3(0, 0, 0));
		float multiplier = 2.0;

		camera_->yaw(Ogre::Degree(relativePoint.getX() * multiplier));
		camera_->pitch(Ogre::Degree(relativePoint.getY() * multiplier));

		camera_->moveRelative(
        Ogre::Vector3(cannula_scene_node_->getPosition().x,
                      cannula_scene_node_->getPosition().y,
                      cannula_scene_node_->getPosition().z + dist));

		float movementMultiplier = 0.1;
		camera_->moveRelative(Ogre::Vector3(0, 0, relativePoint.getZ()* movementMultiplier * dist));
	}

	// finally, update the display
	paintGL();
	updateGL();
}

void GLWidget::setRecording(bool value) {
	recording = value;

	if (value) {
		// Initialize the file to use for recording configurations at each time 
    // step.
		record_file_.open("REPLACE_ME.TXT");

		recordingTimer.start();
	}
}

void GLWidget::setPlayingBack(bool value) {
	playingBack = value;
	recording = false;

	std::string line;

	// clear the queue if not empty
	if (!configurationQueue.empty()) {
		for (size_t i = 0; i < configurationQueue.size(); ++i) {
			configurationQueue.pop();
		}
	}

  // if we should be playing the recording back,
  // read all the data from the file and enqueue all of the configurations
	if (value) {
		std::ifstream file("REPLACE_ME.TXT");

		while (!file.eof()) {
			double configurationArray[4];
			file >> configurationArray[0] >> configurationArray[1] >> configurationArray[2];
			configurationArray[3] = 1;

			Eigen::Vector3d configuration;
			for (size_t i = 0; i < configuration.size(); ++i) {
				configuration(i) = configurationArray[i];
			}

			getline(file, line);
			configurationQueue.push(configuration);
		}
	}
	
}

void GLWidget::set_planning_slot(bool planning) {
  set_is_motion_planning(planning);
}

void GLWidget::draw_shape(const std::vector<Eigen::Vector3d> *vertices,
                          const std::vector<Eigen::Vector3d> *normals,
                          const std::vector<Eigen::Matrix<int, 6, 1>>* faces,
                          const Eigen::VectorXi *tubeStarts,
                          const Eigen::VectorXi *tubeEnds) {
  createCannulaMesh(*vertices, *normals, *faces, *tubeStarts, *tubeEnds);
  paintGL();
  updateGL();
  // free memory
  delete vertices;
  delete normals;
  delete faces;
  delete tubeStarts;
  delete tubeEnds;
}