#ifndef GLWIDGET_H
#define GLWIDGET_H

// Fix for the conflict between Qt's Moc Run and boost header-only libraries
#ifndef Q_MOC_RUN
// Standard
#include <windows.h>
#include <cstdint>
#include <sstream>
#include <fstream>
// For smart pointers
#include <memory> 

// Qt
#include <QGLWidget>
#include <QMessageBox>
#include <Qt/qtimer.h>
#include <QLabel>
#include <QMouseEvent>

// Ogre
#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>
#include <OgreMeshManager.h>
#include <OgreHardwareBufferManager.h>
#include <OgreSubMesh.h>

// Cannula 
#include <cannula/cannula-model/cannula.h>
#include <cannula/cannula-utils/cannula-loader.h>
#include <cannula/cannula-utils/cannula-viz.h>
#include <cannula/cannula-utils/cannula-mesh-gen.h>
// prevent the inclusion of windows.h from messing up std::min and std::max
#undef max
#undef min
#include <cannula/cannula-collisions/cannula-collisions.h>
#include <ompl/cannula/cannula-planning.h>

// Simulation
#include "simulation/simulation.h"
#include "planning/simulation_planner.h"
#include "planning/configurations_queue_manager.h"
#include "util/composite_robot.h"

// Real Cannula
#include "real_cannula/cannula.h"

// OMPL
#include <ompl/control/planners/rrt/RRT.h>
#include <ompl/base/ProblemDefinition.h>

// boost
#include <boost/optional.hpp>
#include <boost/timer/timer.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>

// OpenGL
#include <windows.h>
#include <GL/glu.h>
#include <gl/glu.h>

// Haptic Device (Phantom)
#include <HD/hd.h>
#include <HDU/hduError.h>
#include <HDU/hduVector.h>
#include <HL/hl.h>
#include <HLU/hlu.h>
#endif

// callback functions for asynchronous getting/setting of Phantom attributes
HDCallbackCode HDCALLBACK stateCallback(void *pUserData);
HDCallbackCode HDCALLBACK forceCallback(void *pUserData);

// The DeviceState struct is responsible for storing information about the
// Phantom Haptic Device's state, such as its orientation and position.
struct DeviceState {
  // 6 DOFs = two vectors of size 3 each
	HDdouble position[3];
	HDdouble orientation[3];
	volatile bool isButtonPressed;

  // default constructor
  // initialize isButtonPressed to false
  DeviceState() : isButtonPressed(false) { }
};

// Class used to store and manipulate
// points to manage camera position
class StoragePoint {
public:
	StoragePoint() {
    x = y = z = 0;
	}

	StoragePoint(int x, int y, int z) {
		this->x = x;
		this->y = y;
		this->z = z;
	}

  /*
  * Accessors
  */
	int getX() const { return x; }
	int getY() const { return y; }
	int getZ() const { return z; }

  /*
  * Mutators
  */
	void setX(int x) { this->x = x; }
	void setY(int y) { this->y = y; }
	void setZ(int z) { this->z = z; }

	void setXYZ(int x, int y, int z) {
		setX(x);
		setY(y);
		setZ(z);
	}

	// overload subtraction and addition for convenience
  // of calculating difference between points
	StoragePoint operator-(const StoragePoint& pointTwo) {
		return StoragePoint(this->getX() - pointTwo.getX(), this->getY() - 
                        pointTwo.getY(), this->getZ() - pointTwo.getZ());
	}

	StoragePoint operator+(const StoragePoint& pointTwo) {
		return StoragePoint(this->getX() + pointTwo.getX(),
                        this->getY() + pointTwo.getY(),
                        this->getZ() + pointTwo.getZ());
	}

private:
	int x, y, z;

};

class GLWidget:public QGLWidget
{
Q_OBJECT
 public:
  GLWidget(QWidget *parent = 0);
  ~GLWidget();
	void setRecording(bool value);
	bool getRecording() const { return recording; }

	void setPlayingBack(bool value);
	bool getPlayingBack() const { return playingBack; }

  void set_is_motion_planning(bool is_motion_planning) { is_motion_planning_ = is_motion_planning; }
  bool is_motion_planning() { return is_motion_planning_; }

 protected:
  void initializeGL();
  void paintGL();
  void resizeGL(int width, int height);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);

 private:
  static const size_t kMotorsPerTube = 2;
  static const size_t kMillisecondsPerSecond = 1e3;
  static const size_t kWidth = 1960, kHeight = 1080;
	static const size_t kFps = 24;
  const std::string kVisualizationSettingsFileName;
	// Ogre-related functions
	void configure();
	void createCamera();
	void createViewport();
	void createScene();
  void initializeMeshManager();
  void loadResources();

	// cannula-related functions
	void createCannulaMesh(std::vector<Eigen::Vector3d> vertices,
                         std::vector<Eigen::Vector3d> normals,
                         std::vector<Eigen::Matrix<int, 6, 1>> faces,
                         Eigen::VectorXi tubeStarts,
                         Eigen::VectorXi tubeEnds);
  void initializeCannulaPlanner();
	void initializeSphere(float radius);
	void positionSphere(const Eigen::Vector3d& position) const;
	
  // Simulation & Planning-related functions
  Eigen::Vector3d getGoalConfiguration();
  void initializeCannulaSimulationConnections();
  void initializeCannulaSimulation();
  void initializeCannulaSimulationMotors();
  void read_simulation_settings_file_name();
  void initialize_visualization_settings();

	// Haptic Device (Phantom)-related functions
	void initializeHapticDevice();

	// General variables
	QTimer timer;
	bool isMousePressed, is_motion_planning_, createdCannula, createdScene,
       recording, playingBack;
  std::string simulation_settings_file_name_;
	std::ofstream record_file_;
  std::queue<Eigen::Vector3d> configurationQueue;
  std::unordered_map<std::string, std::string> simulation_settings_;

  // Cannula Simulation Variables
	boost::timer::cpu_timer recordingTimer;
  CannulaSimulation::SimulatedCannula simulated_cannula_;
  CannulaSimulation::Planner simulation_planner_;
  CannulaSimulation::ConfigurationsQueueManager simulation_queue_manager_;
  RealCannula::Cannula real_cannula_;
  CompositeCannula composite_cannula_;

	// Ogre variables and Pointers (see note below).
  // Note about the use of pointers that are point to Ogre instances:
  // OGRE allocates and manages all OGRE object internally, with the exception
  // of the Root object. It is the client code's responsibility to deallocate 
  // only the Root object - otherwise, OGRE will automatically deallocate any
  // objects once the last reference to that object is removed.
  // Source: http://athile.net/library/wiki/index.php/Library/OGRE
	Ogre::Root root_; 
  Ogre::MeshPtr mesh_;
	Ogre::SubMesh *cannula_mesh_;
	Ogre::Camera *camera_;
  Ogre::Viewport *viewport_;
	Ogre::SceneManager *sceneManager;
	Ogre::RenderWindow *renderWindow;
	Ogre::SceneNode *cannula_scene_node_, *sphere_scene_node_;
	Ogre::Light *cannulaLight;

	// Cannula variables
  // Utilize a shared_ptr because loadCannula expects the callee to deallocate
  // the Cannula instance when the callee is done with it.
	std::shared_ptr<CannulaPlanning> cannula_planning_;
	Environment env;
  // TODO: Only one Cannula pointer is required in the new version of the
  // planning code
  std::shared_ptr<Cannula> cannula_for_simulation_, cannula_for_planning_;
  //Cannula cannula_for_simulation_, cannula_for_planning_;

	// Haptic Device (Phantom) variables
	HHD hHD;
	HHLRC hHLRC;
	HDSchedulerHandle stateCallbackHandle;
  // will hold vectors containing the position and the orientation of the Phantom device
	DeviceState deviceState; 

	// Qt variables
	QPoint startPoint;
	StoragePoint myStartPoint; 

 private slots:
	void update();
  void draw_shape(const std::vector<Eigen::Vector3d> *vertices,
                  const std::vector<Eigen::Vector3d> *normals,
                  const std::vector<Eigen::Matrix<int, 6, 1>>* faces,
                  const Eigen::VectorXi *tubeStarts,
                  const Eigen::VectorXi *tubeEnds);
  void set_planning_slot(bool planning);
  
 signals:
  void new_goal_point(Eigen::Vector3d *goal_point);
};

#endif // GLWIDGET_H
