#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QKeyEvent>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit Dialog(QWidget *parent = 0);
    virtual ~Dialog();
    
private:
    Ui::Dialog *ui;

protected:
	void keyPressEvent(QKeyEvent *e);

};

#endif // DIALOG_H
