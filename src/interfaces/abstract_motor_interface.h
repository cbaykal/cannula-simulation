#ifndef CANNULA_VISUALIZATION_SIMULATION_ABSTRACT_MOTOR_INTERFACE_H_
#define CANNULA_VISUALIZATION_SIMULATION_ABSTRACT_MOTOR_INTERFACE_H_

class AbstractMotorInterface {
 public:
  virtual ~AbstractMotorInterface() {}
  
  /*
  * Mutators
  */
  virtual void set_motor_speed(double motor_speed) = 0;
  virtual void set_goal_position(double new_goal_position) = 0;
  virtual void set_position(double position) = 0;

  virtual void go_distance(double delta_distance) = 0;

  /*
  * Accessors
  */
  virtual double position() const = 0;
  virtual double goal_position() const = 0;

  virtual double max_motor_speed() const = 0;
  virtual double motor_speed() const = 0;

  virtual double get_distance_given_elapsed_time(double milliseconds) const = 0;
  virtual bool is_goal_position_reached() const = 0;
};

#endif // CANNULA_VISUALIZATION_SIMULATION_ABSTRACT_MOTOR_INTERFACE_H_