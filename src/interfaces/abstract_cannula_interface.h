#ifndef CANNULA_VISUALIZATION_SIMULATION_ABSTRACT_CANNULA_INTERFACE_H_
#define CANNULA_VISUALIZATION_SIMULATION_ABSTRACT_CANNULA_INTERFACE_H_
#include "util/state_information.h"
#include <qt/qthread.h>
/*
* Interface used to represent the main functionalities
* of a Robot. Main functionality will be to advance
* each of its motors to reach a given goal configuration
*/

// subclass of QObject for slots and signals
class AbstractCannulaInterface: public QThread {
Q_OBJECT
 public: 
  virtual ~AbstractCannulaInterface() {}
  virtual void turn_on_robot() = 0;
  virtual void turn_off_robot() = 0;
  virtual bool is_robot_running() const = 0;

  virtual void add_motor(double max_motor_speed) = 0;
  
 private slots:
  virtual void new_goal_configuration(StateInformation goal_state) = 0;
};

#endif // CANNULA_VISUALIZATION_SIMULATION_ABSTRACT_CANNULA_INTERFACE_H_
