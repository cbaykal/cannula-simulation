#ifndef CANNULA_VISUALIZATION_SIMULATION_MOTOR_SIMULATION_H_
#define CANNULA_VISUALIZATION_SIMULATION_MOTOR_SIMULATION_H_

#include "interfaces/abstract_motor_interface.h"
#include <cmath> // for fabs

namespace CannulaSimulation {
// inner class to represent a Simulated Motor
// extends AbstractMotorInterface
class MotorSimulation : public AbstractMotorInterface {
  public:
  // set the motor maximum speeds in *units* per second
  MotorSimulation(double max_motor_speed = 0)
      : kMaxMotorSpeed(max_motor_speed),
        kZeroCompareValue(1e-9) {
    position_ = goal_position_ = 0;
  }

  /*
  * Mutators
  */
  void set_motor_speed(double motor_speed) {
    motor_speed_ = motor_speed;
  }

  void set_goal_position(double new_goal_position) {
    // if the goal position is less than the current
    // position, we need to go backwards, otherwise
    // we need to advance the motors forwards
    goal_position_ = new_goal_position;
    direction_ = goal_position() > position() ? FORWARD : REVERSE;
  }

  // should not be directly used unless initializing
  // minimum beta insertions
  void set_position(double position) { position_ = position; }

  // advances the motor's position by the given amount; if the given distance
  // is nonpositive, the position will not be changed
  void go_distance(double delta_distance) { 
    if (delta_distance <= 0) { return; }
    if (direction_ == FORWARD) {
      if (position_ + delta_distance >= goal_position()) {
        set_position(goal_position());
      } else {
        position_ += delta_distance;
      }
    } else {
      if (position_ - delta_distance <= goal_position()) {
        set_position(goal_position());
      } else {
        position_ -= delta_distance;
      }
    } // end if direction_ == FORWARD  
  }

  /*
  * Accessors
  */
  double position() const { return position_; }
  double goal_position() const { return goal_position_; }
  double motor_speed() const { return motor_speed_; }
  double max_motor_speed() const { return kMaxMotorSpeed; }

  bool is_goal_position_reached() const {
    double diff = goal_position() - position();

    // do not directly compare two doubles
    // precision errors arise
    return fabs(diff) <= kZeroCompareValue;
  }
  
  double get_distance_given_elapsed_time(double milliseconds) const { 
    return milliseconds*motor_speed();
  }

  private:
  enum kDirection { FORWARD, REVERSE };
  const double kMaxMotorSpeed;
  const double kZeroCompareValue;
      
  kDirection direction_;
  double position_;
  double goal_position_;
  double motor_speed_;
};
} // namespace CannulaSimulation

#endif // CANNULA_VISUALIZATION_SIMULATION_MOTOR_SIMULATION_H_