#include "simulation/simulation.h"
#include <util/so2-distance.h>

CannulaSimulation::SimulatedCannula::SimulatedCannula()
    : is_robot_running_(false), 
      have_set_motor_distance_goals_(false),
      asked_for_goal_state_(false),
      kZeroCompareValue(1e-9),
      goal_state_(0),
      motors_per_tube_(0) {
  // initalize thread
  moveToThread(this);
  // explicity set the elapsed time; calculating it
  // directly was causing nonlinear behavior of the motors
  // elapsed time is now in member variable elapsed_time_milliseconds_
  motor_update_interval_milliseconds_ =
      size_t(kMillisecondsPerSecond/kMotorUpdatesPerSecond);
}

CannulaSimulation::SimulatedCannula::~SimulatedCannula() {
  turn_off_robot();
  // Free all of the motors in the vector. (VC10 doesn't support range based 
  // loops yay).
  for (auto it = motors_vector_.begin(); it != motors_vector_.end(); ++it) {
    delete (*it);
  }

  quit();
  wait();
}

void CannulaSimulation::SimulatedCannula::set_current_u_z(
                                                     const Eigen::VectorXd &current_u_z) {
  // use a mutex here in case the planner is trying to set initial u_z
  QMutexLocker mutex_locker(&mutex_);
  current_u_z_ = current_u_z;
}


void CannulaSimulation::SimulatedCannula::run() {
  // Separate timers for motor simulation and for 
  // notifying the visualization thread of the new shape
  // updating of the motors is more accurate to ensure
  // higher accuracy
  motors_update_timer_ = std::make_shared<QTimer>(this);
  motors_update_timer_->moveToThread(this);
  connect(motors_update_timer_.get(), SIGNAL(timeout()), this, SLOT(update_motor_states()));
  motors_update_timer_->start(int(kMillisecondsPerSecond/kMotorUpdatesPerSecond));

  // notify the visualization thread at a rate of 60 FPS
  // to ensure a smooth display 
  shape_update_timer_ = std::make_shared<QTimer>(this);
  shape_update_timer_->moveToThread(this);
  connect(shape_update_timer_.get(), SIGNAL(timeout()), this, SLOT(notify_visualization_of_new_shape()));
  shape_update_timer_->start(int(kMillisecondsPerSecond/kShapeNotificationsPerSecond));

  // calculate minimumBetaInsertion and initialize Motors
  Eigen::VectorXd minimum_insertion_beta = cannula_->getMinimumInsertBeta();
  initialize_minimum_beta_insertion(minimum_insertion_beta);

  exec();
}

void CannulaSimulation::SimulatedCannula::add_motor(double max_motor_speed) {
  motors_vector_.push_back(new MotorSimulation(max_motor_speed));
}

void CannulaSimulation::SimulatedCannula::initialize_minimum_beta_insertion(const Eigen::VectorXd& vector) {
  for (size_t i = 1; i < motors_vector_.size(); i+=2) {
    // odd numbered motors control beta position
    // iterate over them and set initial positions
    motors_vector_.at(i)->set_position(vector(i/motors_per_tube_));
  }
}

// returns whether all of the Motors have reached
// the specified alpha and beta goal distances.
bool CannulaSimulation::SimulatedCannula::are_all_position_goals_reached() const {
  for (auto it = motors_vector_.begin(); it != motors_vector_.end(); ++it) {
    if (!(*it)->is_goal_position_reached()) {
      return false;
    }
  }

  return true;
}

inline void CannulaSimulation::SimulatedCannula::advance_motor_positions(double elapsed_time_milliseconds) {
  for (size_t i = 0; i < motors_vector_.size(); ++i) {
    if (!motors_vector_.at(i)->is_goal_position_reached()) 
    {
      double distance = 
              motors_vector_.at(i)->get_distance_given_elapsed_time(elapsed_time_milliseconds);

      motors_vector_.at(i)->go_distance(distance);
    }
  }
}

void CannulaSimulation::SimulatedCannula::update_motor_states() {
  // Utilize a mutex in case the planner is trying to set initial u_z guess.
  QMutexLocker mutex_locker(&mutex_);
  if (current_u_z_.size() == 0 || !is_robot_running()) {
    return;
  }

  mutex_locker.unlock();

  // we need a new goal state, so notify 
  // the configurations queue manager
  if (!goal_state_.initialized_information() || goal_state_.state_reached()) {
    if (!asked_for_goal_state_) {
      emit need_goal_configuration();
      // this flag makes sure that we only ask for 
      // the goal once per udpate need
      asked_for_goal_state_ = true;
    }

    return;
  }

  if (!have_set_motor_distance_goals_) {
    set_motor_goal_positions();
    
  } else if (are_all_position_goals_reached()) {
    // we have reached the goal state, so
    // reflect this on the current goal state
    goal_state_.set_state_reached(true);
  } else {
    // if we are here, the motors have been set distance goals,
    // but they haved not reached them yet, so advance distances
    advance_motor_positions(motor_update_interval_milliseconds_);
  }
}

void CannulaSimulation::SimulatedCannula::notify_visualization_of_new_shape() {
  // if the goal state does not have populated vectors
  // do not bother calculating and displaying 
  // the shape
  if (!goal_state_.initialized_information()) { return; }
  // calculate new shape for the 
  // visualization thread to render
  StateInformation current_state = StateInformation::create_state_information_from_motors(
                                                   cannula_->getNumTubes(), motors_per_tube_, motors_vector_);
  
  CannulaShape new_shape;
  bool no_bifurcation = cannula_->calculateShape(current_state.alpha(), 
                                                 current_state.beta(), 
                                                 current_u_z_, &new_shape);

  if (no_bifurcation) {
    // set current u_z guess from the newly calculated shape (will use for subsequent iteration)
    current_u_z_ = new_shape.getBVPSoln();
    prepare_cannula_vectors(&new_shape);
  } else {
    std::cout << "Warning: Bifurcation" << std::endl;
  }
}

// Populates the vectors containing vertex, normals, and faces information
// and passes the vectors along to the visualization thread via a signal
void CannulaSimulation::SimulatedCannula::prepare_cannula_vectors(const CannulaShape* shape) {
  // These will be freed in the visualization thread
  // when shapeReady is emitted
	std::vector<Eigen::Vector3d> *vertices = new std::vector<Eigen::Vector3d>;
	std::vector<Eigen::Vector3d> *normals = new std::vector<Eigen::Vector3d>;
	std::vector<Eigen::Matrix<int, 6, 1>> *faces = new std::vector<Eigen::Matrix<int, 6, 1>>;
  Eigen::VectorXi *tubeStarts = new Eigen::VectorXi(shape->getNumTubes());
	Eigen::VectorXi *tubeEnds = new Eigen::VectorXi(shape->getNumTubes());
	size_t pointsPerCircle = 8;

  // populate vectors using auxillary function
	CannulaUtils::generateCannulaMesh(*shape,
                                    pointsPerCircle,
		                                vertices,
                                    normals,
                                    faces,
		                                tubeStarts,
                                    tubeEnds);
  // pass along the pointers to the populated vectors to the visualization thread
  emit shape_ready(vertices,
                  normals,
                  faces,
                  tubeStarts,
                  tubeEnds);
}

/*
* Returns the maxiumum time required for all of the motors
* to reach their specified goal position
*/
double CannulaSimulation::SimulatedCannula::get_max_time_to_reach_goal() const {
  // find the maximum time required for the motors to reach to goal
  double max_time_to_reach_goal = -std::numeric_limits<double>::infinity();

  for (size_t i = 0; i < motors_vector_.size(); ++i) {
    size_t current_tube = floor(double(i/motors_per_tube_));

    // initialize motor speeds to the maximum possible
    // speed at each iteration since they may have changed
    // in the previous call of setMotorDistanceGoals
    motors_vector_.at(i)->set_motor_speed(motors_vector_.at(i)->max_motor_speed());
    // the evens motors will control alpha distances, and 
    // the odd motors will control beta distances
    if (i % 2 == 0) {
      double delta_alpha_position = so2Distance(motors_vector_.at(i)->position(),
                                                goal_state_.get_alpha_at_tube_index(current_tube));

      double time_required_alpha = fabs(delta_alpha_position/(motors_vector_.at(i)->motor_speed()));

      if (time_required_alpha > max_time_to_reach_goal) {
        max_time_to_reach_goal = time_required_alpha;
      }

    } else {
      double delta_beta_position = goal_state_.get_beta_at_tube_index(current_tube) - motors_vector_.at(i)->position();
      double time_required_beta = fabs(delta_beta_position/(motors_vector_.at(i)->motor_speed()));

      if (time_required_beta > max_time_to_reach_goal) {
        max_time_to_reach_goal = time_required_beta;
      }
    }  
  }

  return max_time_to_reach_goal;
}

void CannulaSimulation::SimulatedCannula::set_motor_goal_positions() {
  double max_time_to_reach_goal = get_max_time_to_reach_goal();

  for (size_t i = 0; i < motors_vector_.size(); ++i) {
    // the first motor will control alpha distances, and 
    // the second motor will control beta distances
    size_t current_tube = floor(double(i/motors_per_tube_));

    if (i % 2 == 0) {
      double delta_alpha_position = so2Distance(motors_vector_.at(i)->position(),
                                                goal_state_.get_alpha_at_tube_index(current_tube));

      motors_vector_.at(i)->set_goal_position(motors_vector_.at(i)->position() +
                                              delta_alpha_position);
      
      // BUG FIX: adjust the speed of the motor so that all of 
      // the motors get to the destination at the same time
      double alpha_speed; // new speed to set
      
      // Bug Fix: consider the case that time required is 0
      // or else, we would be diving by 0 (velocity = distance/time)
      if (max_time_to_reach_goal < kZeroCompareValue) {  
        alpha_speed = 0;
      } else {
        alpha_speed = fabs(delta_alpha_position)/max_time_to_reach_goal;
      }

      motors_vector_.at(i)->set_motor_speed(alpha_speed);

    } else {
      motors_vector_.at(i)->set_goal_position(goal_state_.get_beta_at_tube_index(current_tube));

      // BUG FIX: adjust the speed of the motor so that all of 
      // the motors get to the destination at the same time
      double delta_beta_position = goal_state_.get_beta_at_tube_index(current_tube) -
                                             motors_vector_.at(i)->position();
      double beta_speed;

      // Bug Fix: consider the case that time required is 0
      // or else, we would be diving by 0 (velocity = distance/time)
      if (max_time_to_reach_goal < kZeroCompareValue) {  
        beta_speed = 0;
      } else {
        beta_speed = fabs(delta_beta_position)/max_time_to_reach_goal;
      }
      motors_vector_.at(i)->set_motor_speed(beta_speed);
    }
  }

  have_set_motor_distance_goals_ = true;
}

void CannulaSimulation::SimulatedCannula::new_goal_configuration(StateInformation goal_state) {
  goal_state_ = goal_state;
  goal_state_.set_initialized_information(true);
  asked_for_goal_state_ = have_set_motor_distance_goals_ = false;
}

inline void CannulaSimulation::SimulatedCannula::print_current_state() const {
  typedef StateInformation SI;
  SI current_state = SI::create_state_information_from_motors(
                             cannula_->getNumTubes(),
                             motors_per_tube_,
                             motors_vector_);
  
  std::cout << current_state << std::endl;

  // Alternatively, we can return state
  // would it be useful?
  // return current_state;
}
