#ifndef CANNULA_VISUALIZATION_SIMULATION_SIMULATION_H_
#define CANNULA_VISUALIZATION_SIMULATION_SIMULATION_H_
// Standard
#include <memory>

// Qt
#include <Qt/qthread.h>
#include <Qt/qtimer.h>
#include <Qt/qmutex.h>

// Planning
#include <cannula/cannula-model/cannula.h>
#include <cannula/cannula-utils/cannula-mesh-gen.h>

#ifndef Q_MOC_RUN // boost headers are included here and MOC hates boost
#include <ompl/cannula/cannula-planning.h>
#endif

// Boost
#ifndef Q_MOC_RUN //MOC hates boost
#include <boost/timer/timer.hpp>
#endif

// Eigen
#include <Eigen/Core>

// Necessary simulation-related classes
#include "interfaces/abstract_cannula_interface.h"
#include "util/state_information.h"
#include "simulation/motor_simulation.h"

// Creates a simulation for the concentric tube robot (cannula) 
// advances simulated motors accordingly to reach a given goal
// configuration.
// Example Usage:
// SimulatedCannula simulated_cannula;
// *initialize connections to the queue manager here via
// Qt's signals and slots mechanism*
// simulated_cannla.start()
// Authors: Cenk Baykal, Luis G. Torres

namespace CannulaSimulation {
class SimulatedCannula: public AbstractCannulaInterface {
Q_OBJECT
 public:
  SimulatedCannula();
  ~SimulatedCannula();

  /*
  * Mutators
  */
  virtual void turn_on_robot() { 
    is_robot_running_ = true;
    start();
  }

  virtual void turn_off_robot() { is_robot_running_ = false; }
  
  virtual void add_motor(double max_motor_speed);

  void set_motors_per_tube(size_t motors_per_tube) {
    motors_per_tube_ = motors_per_tube;
  }

  void set_cannula(const std::shared_ptr<Cannula> cannula) { 
    cannula_ = cannula;
  }

  void set_current_u_z(const Eigen::VectorXd &current_u_z);

  /*
  * Accessors
  */
  virtual bool is_robot_running() const { return is_robot_running_; }
  virtual void print_current_state() const;

 protected:
  virtual void run();

 private:
  static const size_t kMillisecondsPerSecond = 1e3;
  static const size_t kMotorUpdatesPerSecond = 200;
  static const size_t kShapeNotificationsPerSecond = 60;
  const double kZeroCompareValue;

  // disallow copy constructor and assignment operator by 
  // putting them in the private section
  SimulatedCannula(const SimulatedCannula&);
  SimulatedCannula& operator=(const SimulatedCannula&);

  void advance_motor_positions(double elapsed_time_milliseconds);

  double get_max_time_to_reach_goal() const;
  bool are_all_position_goals_reached() const;
  void initialize_minimum_beta_insertion(const Eigen::VectorXd& vector);
  void prepare_cannula_vectors(const CannulaShape* shape);
  void set_motor_goal_positions();

  QMutex mutex_;
  bool is_robot_running_;
  bool have_set_motor_distance_goals_;
  bool asked_for_goal_state_;
  double motor_update_interval_milliseconds_;

  std::shared_ptr<QTimer> motors_update_timer_, shape_update_timer_;
  StateInformation goal_state_;
  std::vector<AbstractMotorInterface *> motors_vector_;
  size_t motors_per_tube_;

  /*
  * Cannula-related Variables
  */
  std::shared_ptr<Cannula> cannula_;
  Eigen::VectorXd current_u_z_;

 private slots:
  virtual void new_goal_configuration(StateInformation goal_state);

  void notify_visualization_of_new_shape();
  void update_motor_states();
  
 signals:
  void need_goal_configuration();

  void shape_ready(const std::vector<Eigen::Vector3d> *vertices,
                   const std::vector<Eigen::Vector3d> *normals,
                   const std::vector<Eigen::Matrix<int, 6, 1>>* faces,
                   const Eigen::VectorXi *tubeStarts,
                   const Eigen::VectorXi *tubeEnds);
};
} // namespace CannulaSimulation

#endif // CANNULA_VISUALIZATION_SIMULATION_SIMULATION_H_