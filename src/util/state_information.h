#ifndef CANNULA_VISUALIZATION_SIMULATION_STATE_INFORMATION_H_
#define CANNULA_VISUALIZATION_SIMULATION_STATE_INFORMATION_H_
#include <Eigen/Core>
#include <vector>
#include <iostream>
#include <Qt/QMetaType.h>

// class to keep track of alpha and beta values (translation and rotation)
// of each state in the collision-free plan
class StateInformation {
 public:
  // provide default constructor for QMetaType
  StateInformation() {
    alpha_.resize(0);
    beta_.resize(0);
  }

  StateInformation(size_t num_tubes)
      : state_reached_(false),
        initialized_information_(false) {
    alpha_.resize(num_tubes);
    beta_.resize(num_tubes);
  }

  // static method to create a StateInformation object from the
  // current state of the motors
  template <typename MotorContainer>
  static StateInformation create_state_information_from_motors(size_t num_tubes_,
                                                               size_t kMotorsPerTube,
                                                               const MotorContainer& motors) {
    StateInformation state_information(num_tubes_);

    for (size_t motor_index = 0; motor_index < motors.size(); ++motor_index) {
      size_t current_tube = floor(double(motor_index/kMotorsPerTube));

      double position = motors.at(motor_index)->position();

      if (motor_index % 2 == 0) {
        state_information.add_alpha_to_state(current_tube, position);
      } else {
        state_information.add_beta_to_state(current_tube, position);
      }
    }

    return state_information;
  }

  /*
  * Mutators
  */
  void add_alpha_to_state(size_t tube_index, double alpha_position) { alpha_(tube_index) = alpha_position; }
  void add_beta_to_state(size_t tube_index, double beta_position) { beta_(tube_index) = beta_position; }

  void set_state_reached(bool state_reached) { state_reached_ = state_reached; }
  void set_initialized_information(bool initialized_information) { initialized_information_ = initialized_information; }

  /*
  * Accessors
  */
  double get_alpha_at_tube_index(size_t tube_index) const { return alpha_(tube_index); }
  double get_beta_at_tube_index(size_t tube_index) const { return beta_(tube_index); }

  Eigen::VectorXd alpha() const { return alpha_; }
  Eigen::VectorXd beta() const { return beta_; }

  bool state_reached() const { return state_reached_; }
  bool initialized_information() const { return initialized_information_; }
  // overloading operator<< does not play well with OMPL for some reason
  // Proposed fix: Put StateInformation in a namespace
  friend std::ostream& operator<<(std::ostream& os, const StateInformation &obj) {
    for (size_t i = 0; i < obj.alpha_.size(); ++i) {
      os << "Alpha( " << i << "): " << obj.alpha_(i) << std::endl;
      os << "Beta( " << i << "): " << obj.beta_(i) << std::endl << std::endl;
    }

    return os;
  }

 private:
  // VectorXd(i) will yield alpha or beta for tube number i
  Eigen::VectorXd alpha_;
  Eigen::VectorXd beta_;
  bool state_reached_, initialized_information_;
};

Q_DECLARE_METATYPE(StateInformation);

#endif // CANNULA_VISUALIZATION_SIMULATION_STATE_INFORMATION_H_