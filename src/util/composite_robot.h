#ifndef CANNULA_VISUALIZATION_SIMULATION_COMPOSITE_ROBOT_H_
#define CANNULA_VISUALIZATION_SIMULATION_COMPOSITE_ROBOT_H_
#include "interfaces/abstract_cannula_interface.h"
#include <vector>
/*
* Class CompositeRobot uses the composite design pattern to
* contain multiple objects that implement the AbstractCannulaInterface;
* CompositeCannula subscribes to signals and calls the appropriate 
* methods for all of these objects in response to events 
* (i.e. new goal configuration is set by the user)
* Authors: Cenk Baykal, Luis G. Torres
*/
class CompositeCannula : public AbstractCannulaInterface {
Q_OBJECT

 public:
  CompositeCannula() {
    // Need this for signals/slots
    qRegisterMetaType<StateInformation>();  
  }
  
  void add_cannula_interface(AbstractCannulaInterface *cannula_interface) {
    cannula_interfaces_.push_back(cannula_interface);  
  }

  virtual void turn_on_robot() {
    for (auto it = cannula_interfaces_.begin(); it != cannula_interfaces_.end(); ++it) {
      (*it)->turn_on_robot();
    }
  }

  virtual void turn_off_robot() {
    for (auto it = cannula_interfaces_.begin(); it != cannula_interfaces_.end(); ++it) {
      (*it)->turn_off_robot();
    }
  }

  virtual bool is_robot_running() const {
    for (auto it = cannula_interfaces_.begin(); it != cannula_interfaces_.end(); ++it) {
      if (!(*it)->is_robot_running()) {
        return false;
      }
    }

    return true;
  }

  virtual void add_motor(double max_motor_speed) {
    for (auto it = cannula_interfaces_.begin(); it != cannula_interfaces_.end(); ++it) {
      (*it)->add_motor(max_motor_speed);
    }
  }

 private:
  std::vector<AbstractCannulaInterface *> cannula_interfaces_;

 private slots:
  virtual void new_goal_configuration(StateInformation goal_state) {
    for (auto it = cannula_interfaces_.begin(); 
         it != cannula_interfaces_.end(); ++it) {
      QMetaObject::invokeMethod(*it, 
                                "new_goal_configuration",
                                Qt::QueuedConnection,
                                Q_ARG(StateInformation, goal_state));
    }
  }
  
 signals:
  void need_goal_configuration();
  
};
#endif // CANNULA_VISUALIZATION_SIMULATION_COMPOSITE_ROBOT_H_