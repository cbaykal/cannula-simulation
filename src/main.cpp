#include "visualization/dialog.h"
#include "visualization/glwidget.h"
#include <QApplication>
#include <QVBoxLayout>

int main(int argc, char *argv[]) {
	try
	{
	QApplication app(argc, argv);
  Dialog w;
  w.show();

  qRegisterMetaType<StateInformation>();
  return app.exec();
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
		std::cin.get();
	}
}
