find_package(Qt4 COMPONENTS QtGui QtOpenGL REQUIRED)

set(sources src/main.cpp src/visualization/dialog.cpp src/visualization/glwidget.cpp src/simulation/simulation.cpp src/planning/simulation_planner.cpp src/planning/configurations_queue_manager.cpp src/real_cannula/cannula.cpp)
set(headers src/visualization/dialog.h src/visualization/glwidget.h src/simulation/simulation.h src/planning/simulation_planner.h src/planning/configurations_queue_manager.h src/interfaces/abstract_cannula_interface.h src/util/composite_robot.h src/real_cannula/cannula.h src/real_cannula/real_motor.h)
set(forms src/visualization/dialog.ui)

# enable the OpenGL Module
set(QT_USE_OPENGL TRUE)

# invoke moc
qt4_wrap_cpp(headers_moc ${headers})

# wrap UI
qt4_wrap_ui(forms_headers ${forms})

include(${QT_USE_FILE})
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src)
include_directories(${QT_INCLUDE_DIR} ${OGRE_INCLUDE_DIR})

# for ui_dialog.h
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src/visualization)

# Phantom headers
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/phantom/include)

# Generate resources.cfg that knows about lab_data
file(TO_NATIVE_PATH "${DATA_DIR}/meshes" MESHES_DIR_NATIVE_PATH)
file(TO_NATIVE_PATH "${DATA_DIR}/meshes/materials" MATERIALS_DIR_NATIVE_PATH)
configure_file(dist/resources.cfg.in dist/resources.cfg)


add_definitions(${QT_DEFINITIONS})

# OGRE STUFF
if(WIN32)
	set(CMAKE_MODULE_PATH "$ENV{OGRE_HOME}/CMake/;${CMAKE_MODULE_PATH}")
	set(OGRE_SAMPLES_INCLUDEPATH
		$ENV{OGRE_HOME}/Samples/include)
endif(WIN32)
 
if(UNIX)
	if(EXISTS "/usr/local/lib/OGRE/cmake")

	  set(CMAKE_MODULE_PATH "/usr/local/lib/OGRE/cmake/;${CMAKE_MODULE_PATH}")
	  set(OGRE_SAMPLES_INCLUDEPATH "/usr/local/share/OGRE/samples/Common/include/") # We could just *assume* that developers uses this basepath : /usr/local

	elseif(EXISTS "/usr/lib/OGRE/cmake")

	  set(CMAKE_MODULE_PATH "/usr/lib/OGRE/cmake/;${CMAKE_MODULE_PATH}")
	  set(OGRE_SAMPLES_INCLUDEPATH "/usr/share/OGRE/samples/Common/include/") # Otherwise, this one

	else ()
	  message(SEND_ERROR "Failed to find module path.")
	endif(EXISTS "/usr/local/lib/OGRE")
endif(UNIX)
 
if (CMAKE_BUILD_TYPE STREQUAL "")
  # CMake defaults to leaving CMAKE_BUILD_TYPE empty. This screws up
  # differentiation between debug and release builds.
  set(CMAKE_BUILD_TYPE "RelWithDebInfo" CACHE STRING "Choose the type of build, options are: None (CMAKE_CXX_FLAGS or CMAKE_C_FLAGS used) Debug Release RelWithDebInfo MinSizeRel." FORCE)
endif ()
 
set(CMAKE_DEBUG_POSTFIX "_d")
 
find_package(OGRE REQUIRED)
 
#if(NOT "${OGRE_VERSION_NAME}" STREQUAL "Cthugha")
#  message(SEND_ERROR "You need Ogre 1.7 Cthugha to build this.")
#endif()
 
find_package(OIS REQUIRED)
 
if(NOT OIS_FOUND)
	message(SEND_ERROR "Failed to find OIS.")
endif()
 
 
include_directories( ${OIS_INCLUDE_DIRS}
	${OGRE_INCLUDE_DIRS}
	${OGRE_SAMPLES_INCLUDEPATH}
)
 
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/dist/bin)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/dist/media)

##########################################################
# Targets

add_executable(integrated_planning_system
	${sources}
	${headers_moc}
	${forms_headers})

#set_target_properties(integrated_planning_system PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${CMAKE_INSTALL_PREFIX}/dist/bin")

target_link_libraries(integrated_planning_system
		${QT_LIBRARIES}
		${OGRE_LIBRARIES}
		${OPENGL_LIBRARIES}
		${Boost_LIBRARIES}
		cannula-planning
		${CMAKE_CURRENT_SOURCE_DIR}/phantom/lib/hd.lib
		${CMAKE_CURRENT_SOURCE_DIR}/phantom/lib/hdu.lib
		${CMAKE_CURRENT_SOURCE_DIR}/phantom/lib/hl.lib
		${CMAKE_CURRENT_SOURCE_DIR}/phantom/lib/hlu.lib)

if(WIN32)
	install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/phantom/lib/hd.dll
			${CMAKE_CURRENT_SOURCE_DIR}/phantom/lib/hl.dll
			DESTINATION bin
			CONFIGURATIONS Release RelWithDebInfo)
else()
	message(FATAL "Haha this isn't gonna work")
endif(WIN32)
#########################################################
# MORE OGRE STUFF FOR INSTALL
# post-build copy for win32
if(WIN32 AND NOT MINGW)
	#add_custom_command( TARGET integrated_planning_system PRE_BUILD
		#COMMAND if not exist .\\dist\\bin mkdir .\\dist\\bin )
	#add_custom_command( TARGET integrated_planning_system POST_BUILD
		#COMMAND copy \"$(TargetPath)\" .\\dist\\bin )
	
	# gotta copy some stuff to the executable's directory!
	file(TO_NATIVE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/dist/plugins.cfg" PLUGINS_FILE_WINDOWS)
	file(TO_NATIVE_PATH "${CMAKE_CURRENT_BINARY_DIR}/dist/resources.cfg" RESOURCES_FILE_WINDOWS)
	file(TO_NATIVE_PATH "${CMAKE_CURRENT_BINARY_DIR}/Release" EXE_WINDOWS_PATH)
	add_custom_command( TARGET integrated_planning_system POST_BUILD
		COMMAND copy \"${PLUGINS_FILE_WINDOWS}\" \"${EXE_WINDOWS_PATH}\" & copy \"${RESOURCES_FILE_WINDOWS}\" \"${EXE_WINDOWS_PATH}\")
	#file(TO_NATIVE_PATH "${OGRE_PLUGIN_DIR_REL}" OGRE_PLUGIN_WINDOWS_PATH)
	#add_custom_command( TARGET integrated_planning_system POST_BUILD
		#COMMAND robocopy \"${DIST_WINDOWS_PATH}\" \"${EXE_WINDOWS_PATH}\" /e /njh /njs /ndl /nc /ns /np /nfl || exit /b 0)
	#add_custom_command( TARGET integrated_planning_system POST_BUILD
	#	COMMAND copy ${OGRE_PLUGIN_WINDOWS_PATH}\\OgreMain.dll ${EXE_WINDOWS_PATH})
	#add_custom_command( TARGET integrated_planning_system POST_BUILD
	#	COMMAND copy ${OGRE_PLUGIN_WINDOWS_PATH}\\RenderSystem_GL.dll ${EXE_WINDOWS_PATH})
	#add_custom_command( TARGET integrated_planning_system POST_BUILD
	#	COMMAND copy ${OGRE_PLUGIN_WINDOWS_PATH}\\OIS.dll ${EXE_WINDOWS_PATH})
endif(WIN32 AND NOT MINGW)

if(MINGW OR UNIX)
	message(FATAL "Don't build this in UNIX yet idiot")
	#set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/dist/bin)
endif(MINGW OR UNIX)

